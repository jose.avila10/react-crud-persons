import React from 'react';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

import './App.css';
import Persons from './Components/Persons';

function App() {

  return (
    <Router>
      <Routes>
        <Route exact path="/persons" element={<Persons />} />
      </Routes>
    </Router>
  );
}

export default App;
