import React, { useState, useEffect } from 'react';

const EditPersonForm = (props) => {
    const [showFormEdit, setShowFormEdit] = useState(false)

    const [editNameInput, setEditNameInput] = useState('');
    const [editLastnameInput, setEditLastnameInput] = useState('');
    const [editAdressInput, setEditAdressInput] = useState('');
    const [editCityInput, setEditCityInput] = useState('');

    // setEditNameInput(props.personToEdit[0].name);
    // setEditLastnameInput(props.personToEdit[0].lastname);
    // setEditAdressInput(props.personToEdit[0].adress);
    // setEditCityInput(props.personToEdit[0].city);

    // var nameInp = "";
    // if (props.personToEdit.length !== 0){
    //     nameInp = props.personToEdit[0].name;
    // }

    const handleChangeEditName = (event) => {
        setEditNameInput(event.target.value);
    }

    const handleChangeEditLastname = (event) => {
        setEditLastnameInput(event.target.value);
    }

    const handleChangeEditAdress = (event) => {
        setEditAdressInput(event.target.value);
    }
    
    const handleChangeEditCity = (event) => {
        setEditCityInput(event.target.value);
    }

    const submitEdit = () => {
        if (editNameInput !== "" && editLastnameInput !== "" && editAdressInput !== "" && editCityInput !== ""){
            var idToEdit = "none"
            if (props.personToEdit.length !== 0) idToEdit = Number(props.personToEdit[0].id);

            const editedPerson = {
                "name": editNameInput,
                "lastname": editLastnameInput,
                "adress": editAdressInput,
                "city": editCityInput
            };

            props.edit(idToEdit, editedPerson);

        } else {
            alert("Please, fill all fields on the form");
        }
    }

    const cancelEdit = () => {
        // Clears state and therefore the name above (Editing: ....)
        props.clearEdit();
    }

    useEffect(() => {
        if(props.personToEdit.length !== 0){
            setShowFormEdit(true);

            setEditNameInput(props.personToEdit[0].name);
            setEditLastnameInput(props.personToEdit[0].lastname);
            setEditAdressInput(props.personToEdit[0].adress);
            setEditCityInput(props.personToEdit[0].city);
        } else { //To clear inputs after reading en empty state personToEdit
            setShowFormEdit(false);

            setEditNameInput('');
            setEditLastnameInput('');
            setEditAdressInput('');
            setEditCityInput('');
        }
    }, [props])
    
    var name = "";
    if (props.personToEdit.length !== 0) name = props.personToEdit[0].name + " " + props.personToEdit[0].lastname;

    return(
        <div>
            {showFormEdit &&
                <div>
                    <div>
                    <h2>Editing to: {name}</h2>
                    </div>
    
                    <div>
                        <label>Name</label>
                        <input type="text" className="form-control" value={editNameInput} onChange={handleChangeEditName}/>
                        <label>Lastname</label>
                        <input type="text" className="form-control" value={editLastnameInput} onChange={handleChangeEditLastname} />
                        <label>Adress</label>
                        <input type="text" className="form-control" value={editAdressInput} onChange={handleChangeEditAdress} />
                        <label>City</label>
                        <input type="text" className="form-control" value={editCityInput} onChange={handleChangeEditCity} />
                        <br />
                        <button type="button" className="btn btn-primary" onClick={submitEdit}>Save Changes</button>
                        <button type="button" className="btn btn-secondary mx-2" onClick={cancelEdit}>Cancel</button>
                    </div> 
                </div>
            }
        </div>
    )
}

export default EditPersonForm;