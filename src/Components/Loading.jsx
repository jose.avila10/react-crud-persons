import React from 'react';
import './styles/Loading.css';

import Loader from "react-loader-spinner";

const Loading = () => {
    return (
        <div className="LoadingStyle">
            <Loader
                type="Circles"
                color="#00BFFF"
                height={100}
                width={100}
                timeout={0} //Number in Ms, (0 is infinite loop)
            />
        </div>
    );
}

export default Loading;