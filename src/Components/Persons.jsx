import React, { useState, useEffect } from 'react';
import axios from 'axios';

import Loading from './Loading';
import PersonsTable from './PersonsTable';
import NewPersonForm from './NewPersonForm';
import EditPersonForm from './EditPersonForm';

const Persons = () => {

    const [LoadingStatus, setLoadingStatus] = useState(true);
    const [persons, setPersons] = useState([]);
    
    const [dataPersonToEdit, setDataPersonToEdit] = useState([]);

    useEffect(() => {
      fetch("http://localhost:3001/persons")
      .then(response => response.json())
      .then(data => setPersons(data))
      .then(setLoadingStatus(false))
    },[])

    const addPerson = (newPerson) => {
        setLoadingStatus(true);

        axios({
            method: 'post',
            url: 'http://localhost:3001/persons',
            data: newPerson
        })
        .then(res => {
            let newPersonsArray = [...persons, res.data];
            setPersons(newPersonsArray);
        })

        setLoadingStatus(false);
    }

    const deletePerson = (id) => {
        setLoadingStatus(true);

        axios({
            method: 'delete',
            url: 'http://localhost:3001/persons/' + id
        })
        .then( () => {
            const arrayPersonsAfterDelete = persons.filter(person  => person.id !== id);
            setPersons(arrayPersonsAfterDelete);
            }
        )

        setLoadingStatus(false);
    }

    const selectPersontoEdit = (personToEdit) => {
        setDataPersonToEdit(personToEdit);
    }

    const clearPersonToEdit = () => {
        setDataPersonToEdit([]);
    }

    const editPerson = (id, personUpdated) => {
        setLoadingStatus(true);
        if(id === "none"){
            alert("You haven't selected any user to edit");
            setLoadingStatus(false);
            return;
        }
        //continue process
        axios({
            method: 'put',
            url: 'http://localhost:3001/persons/' + id,
            data: personUpdated
        })
        .then(res => {
            const modifiedPerson = {id: id, ...res.data,};
            for(let i = 0; i < persons.length; i++) {
                if(persons[i].id === id){
                    persons[i] = modifiedPerson;
                }
            }
            setPersons([]); //I need to delete old array
            setPersons(persons); //Write new Array using the new values for the updated person
            setDataPersonToEdit([]);

        })
        setLoadingStatus(false);
    }

    return (
        <div>
            {LoadingStatus && <Loading />}

            {!LoadingStatus &&
                <div className="w-75 mx-auto my-2">
                    <NewPersonForm addPerson={addPerson} />
                </div>
            }

            {!LoadingStatus && 
                <div className="w-75 mx-auto my-2">
                    <EditPersonForm personToEdit={dataPersonToEdit} edit={editPerson} clearEdit={clearPersonToEdit}/>
                </div>
            }

            {!LoadingStatus && 
                <div className="w-75 mx-auto my-2">
                    <PersonsTable persons={persons} delete={deletePerson} edit={selectPersontoEdit}/>
                </div>
            }

        </div>
    )
}

export default Persons;