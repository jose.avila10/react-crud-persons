import React, { useState } from 'react';

const NewPersonForm = (props) => {
    const [showForm, setShowForm] = useState(false);

    const [nameInput, setNameInput] = useState('');
    const [lastnameInput, setLastnameInput] = useState('');
    const [adressInput, setAdressInput] = useState('');
    const [cityInput, setCityInput] = useState('');

    const handleChangeName = (event) => {
        setNameInput(event.target.value);
    }

    const handleChangeLastname = (event) => {
        setLastnameInput(event.target.value);
    }

    const handleChangeAdress = (event) => {
        setAdressInput(event.target.value);
    }
    
    const handleChangeCity = (event) => {
        setCityInput(event.target.value);
    }

    const submitButton = () => {
        if (nameInput !== "" && lastnameInput !== "" && adressInput !== "" && cityInput !== ""){
            const newPerson = {
                "name": nameInput,
                "lastname": lastnameInput,
                "adress": adressInput,
                "city": cityInput
            };
            props.addPerson(newPerson);

            setNameInput('');
            setLastnameInput('');
            setAdressInput('');
            setCityInput('');

            setShowForm(false);
        } else {
            alert("Please, fill all fields on the form");
        }
    }

    const cancelAdd = () => {
        setShowForm(!showForm)

        setNameInput('');
        setLastnameInput('');
        setAdressInput('');
        setCityInput('');
    }

    const showOrHide = showForm ? "d-block my-2" : "d-none my-2";

    return(
        <div>
            {!showForm &&
                <div>
                    <button className="btn btn-primary" onClick={() => setShowForm(!showForm)}>Add a new person</button>
                </div>
            }
            {showForm &&
                <h2>New person information</h2>
            }

            <div className={showOrHide}>
                <label>Name</label>
                <input type="text" className="form-control" value={nameInput} onChange={handleChangeName}/>
                <label>Lastname</label>
                <input type="text" className="form-control" value={lastnameInput} onChange={handleChangeLastname} />
                <label>Adress</label>
                <input type="text" className="form-control" value={adressInput} onChange={handleChangeAdress} />
                <label>City</label>
                <input type="text" className="form-control" value={cityInput} onChange={handleChangeCity} />
                <br />
                <button type="button" className="btn btn-primary" onClick={submitButton}>Add Person</button>
                <button type="button" className="btn btn-secondary mx-2" onClick={cancelAdd}>Cancel</button>
            </div>
        </div>
    )
}

export default NewPersonForm;