import React from 'react';

const PersonsTable = (props) => {

    const clickDelete = (id) => {
        if (window.confirm("Are you sure you want to delete this person?")){
            props.delete(id);
        }
    }

    const clickEdit = (id) => {
        const personToEdit = props.persons.filter(person  => person.id === id);
        props.edit(personToEdit);
    }

    return (
        <div>
            <table className="table table-hover">
                <thead className="table-dark">
                    <tr>
                        <th>Fullname</th>
                        <th>Adress</th>
                        <th>City</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {props.persons.map(person => (
                        <tr key={person.id}>
                            <td>{person.name} {person.lastname}</td>
                            <td>{person.adress}</td>
                            <td>{person.city}</td>
                            <td>
                                <button type="button" className="btn btn-primary btn-sm mx-1" onClick={() => clickEdit(person.id)}>Edit</button>
                                <button type="button" className="btn btn-danger btn-sm mx-1" onClick={() => clickDelete(person.id)}>Delete</button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
}

export default PersonsTable;