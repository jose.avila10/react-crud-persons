import React from 'react';
import './styles/Person.css';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';

//This Component is never used
const Person = (props) => {

    const clickDelete = (id) => {
        props.deletePerson(id);
    }

    return (
        <div className="personCard" key={props.info.id}>
            Fullname: {props.info.name} {props.info.lastname} <br />
            Adress: {props.info.adress} <br />
            City: {props.info.city}
            <div className="personButtons">
                <button className="btn btn-primary">Edit</button>
                <button className="btn btn-danger" type="button" onClick={() => clickDelete(props.info.id)}>Delete</button>
            </div>
        </div>
    )
}

export default Person;